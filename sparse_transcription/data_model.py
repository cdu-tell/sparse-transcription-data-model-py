from typing import List, Dict, Tuple
import datetime
import hashlib

class STProject:
    """A Sparse Transcription project is a collection Timelines which share a Glossary
    """
    def __init__(self, glossary):
        self.glossary : 'Glossary' = glossary
        self.timelines : Dict['Timeline'] = {}

    def create_timeline(self, id : str, media : 'List[STMedia]') -> None:
        """Add a timeline to the project. A project can consist of multiple timelines.
        Args:
            timeline (Timeline): [description]
        """
        new_timeline = Timeline(identifier = id, media = media)
        self.timelines[id] = new_timeline

    @staticmethod
    def transcript(timeline : 'Timeline') -> None:
        """IGT_1 view with audio and glossary links, of bg.tokens() for each bg in timeline.get_breath_groups()

        Args:
            timeline (Timeline): [description]
        """
        raise NotImplementedError

class Timeline:
    """ 
    Timelines represent sessions, where many different media may have recorded the same language event.
    A timeline contains multiple media, and is annotated by breath groups which may correspond to the same stretch of time accross multiple media 
    """
    def __init__(self, identifier, media):
        self.identifier : str = identifier
        self.media : List['STMedia'] = media
        self.breath_groups : List['BreathGroup'] = []
        self.interperetations : list = []

    def add_media(self, media : 'STMedia') -> None:
        """add a TSMedia object to the timeline's media collection

        Args:
            media (STMedia): [description]
        """
        self.media.append(media)

    def add_breath_group(self, start : float, end : float):
        # raise error if overlap with existing breathgroup
        for bg in self.breath_groups:
            if (start > bg.start and start < bg.end) or (end < bg.end and end > bg.start):
                raise Exception("Existing breathgroup overlaps with span ", start, end)
        # add breath group to collection
        self.breath_groups.append(BreathGroup(self, start, end))
    
    def get_breath_groups(self) -> List['BreathGroup']:
        """The list of sorted breath groups in this Timeline 

        Returns:
            List[BreathGroup]: [description]
        """
        return sorted(self.breath_groups)

    @staticmethod
    def concordance(glossary_entry : 'GlossaryEntry') -> None:
        """concordance view of bg.tokens(), for each bg in ge.breath_groups())

        Args:
            ge (GlossaryEntry): [description]
        """
        toks = []
        breath_groups = glossary_entry.breath_groups()
        for b in breath_groups:
            print(b)
            toks.append(b.tokens())
        return toks
    
class STMedia:
    """A Sparse Transcription Media instance contains an individual recording, and its related metadata.
    A particular media object has minimally an id, a pointer to the binary file in the datastore, and a timestamp to orient it along a timeline.
    """
    def __init__(self, id, uri, frames, timestamp,  metadata) -> None:
        self.id : str = id
        self.uri : str = uri
        self.timestamp : datetime = timestamp
        self.frames : list = frames
        self.metadata : dict = metadata
    
    def __len__(self):
        return len(self.frames) / float(self.metadata['framerate'])
    
    def change_timestamp(self, year : int, month : int, day : int, hour : int, minute : int, second : int, microsecond : int = 0) -> None:
        """Modify the Media file's timestamp

        Args:
            year (int): The year
            month (int): The month (1 <= month <= 12)
            day (int): The day (1 <= day <= number of days in the given month and year)
            hour (int): The hour (0 <= hour < 24)
            minute (int): The minute (0 <= minute < 60)
            second (int): The second (0 <= second < 60)
            microsecond (int, optional): 0 <= microsecond < 1000000. Defaults to 0.
        """
        self.timestamp = datetime.datetime(year, month, day, hour, minute, second, microsecond)
    
    def playback(self) -> None:
        """ Play back the media file defined
        """ 
        raise NotImplemented

class BreathGroup:
    """A breathgroup is an annotation of the Timeline, representing the time spans (possibly accross multiple media recordings)
    corresponding to meaningful utterances on which we would like to annotate linguistically in a session of the sparse annotation activity.
    """
    def __init__(self, timeline, start = None, end = None):
        self.timeline : 'Timeline' = timeline
        self._tokens : list = []
        self.start : int = start
        self.end : int = end
        self.size : int = start - end
        self.excluded_spans : List[Dict[str, float]]= []

    def __lt__(self, other : 'BreathGroup') -> bool:
        return (self.start < other.start)

    def sparse_transcribe(self, start : float, end : float, form : str, gloss : str, confidence : float, glossary : 'Glossary') -> None:
        """Create an entry for the given span in the glossary and link to it

        Args:
            start (float): start of the Glossary entry in seconds
            end (float): End of the glossary entry in seconds
            form (str): The string representing the glossary entry
            gloss (str): The string representing the glossary entry
        """
        # make sure intended region to transcribe does not exceed breath group boundaries
        if not self.start <= start and self.end >= end:
            raise Exception("Transcription region exceeds bounds of the BreathGroup")

        # generate unique glossary entry id based on the form and gloss
        id = hashlib.md5(str(form+gloss).encode('utf-8')).hexdigest()

        # Check if this entry exists in the glossary; if it does, add the glossary version to the breath group
        ge = glossary.get_entry(id)
        if ge:
            # create a token for this span
            token = Token(
                glossary_entry = ge,
                breath_group = self, 
                confidence = confidence,
                start = start,
                end = end 
                )
            token.set_time(end-start)
            # add this token to the existing glossary entry
            ge._tokens.append(token)

        else: # Create new glossary entry to put in the Glossary 
            new_glossary_entry = GlossaryEntry(form = form, gloss = gloss)
            # create a token for this span
            token = Token(
                glossary_entry = new_glossary_entry,
                breath_group = self, 
                confidence = confidence,
                start = start,
                end = end 
                )
            token.set_time(end-start)
            # add this token to the new glossary entry
            new_glossary_entry._tokens.append(token)
            # add this glossary entry to the glossary
            glossary.add_entry(new_glossary_entry, id = id) 
        
        # Add this token to the BreathGroup
        self._tokens.append(token)

    def add_interperetation(self, start : float, end : float, context : str):
        """Assign [t1, t2] on self.timeline the str value of 'context' 

        Args:
            t1 (float): [description]
            t2 (float): [description]
            context (str): [description]
        """
        # Make sure the annotation occurs in the bounds of the breath group
        if start >= self.start and end <= self.end:
            self.timeline.interperetations.append(Interperetation(start, end, self.timeline, context))
        else:
            raise Exception("Cannot add interperetation to span exceeding bounds of the breathgroup")

    def exclude(self, start : float, end : float):
        """Mark interval excluded from the transcription

        Args:
            start (float): The starting offset, in seconds
            end (float): The stopping offset, in seconds
        """
        self.excluded_spans.append({
            "start": start,
            "end": end
            })

    def get_size(self) -> float:
        """The duration of speech activity in the BreathGroup, omitting any excluded regions

        Returns:
            float: Duration in seconds
        """
        return NotImplementedError
    
    def tokens(self) -> List['Token']:
        """The sorted tokens discovered in the BreathGroup

        Returns:
            List[Token]: Sorted Tokens discovered in this BreathGroup
        """
        return sorted(self._tokens, key=lambda x: x.start)
    
    def collisions(self, overlap_time : float) -> List[List['Token']]:
        """Tokens with overlap greater than overlap_time (float, seconds)

        Args:
            token (float): [description]

        Returns:
            List[List[Token]]: [description]
        """
        return NotImplementedError
    
    def shortfall(self) -> float:
        """Get the sum of the size of all tokens in the BreathGroup minus the size of the breathgroup

        Returns:
            float: The time length of speech in the breathgroup not covered by existing tokens
        """
        return NotImplementedError

class Glossary():
    """A Glossary is the collection of transcribed units which can appear accross multiple timelines in the scope of a project.
    An entry in the Glossary should point to offset in a Timeline where the token occurs.
    """
    def __init__(self):
        self.entries : dict = dict()

    def add_entry(self, ge : 'GlossaryEntry', id : str) -> None:

        """Add an entry to the glossary

        Args:
            form (str): [description]
            gloss (str): [description]
            id (str, optional): [description]. Defaults to None.
        """
        self.entries[id] = ge

    def get_entry(self, id : str) -> 'GlossaryEntry':
        """Retrieve an entry from the glossary

        Args:
            root ([type]): [description]

        Returns:
            GlossaryEntry: [description]
        """
        if id in self.entries:
            return self.entries[id]
        return None

    def similar(self, glossary_entry : 'GlossaryEntry', distance : float) -> List['GlossaryEntry']:
        """Get similar entries within distance of GlossaryEntry

        Args:
            ge (GlossaryEntry): [description]
            distance (float): the allowable distance from the GlossaryEntry

        Raises:
            NotImplemented: [description]
        """
        raise NotImplementedError

    def cluster(self, glossary_entry: 'GlossaryEntry') -> List[List['GlossaryEntry']]:
        """Cluster entries based on form and gloss

        Args:
            ge (GlossaryEntry): [description]

        Raises:
            NotImplementedError: [description]

        Returns:
            List[List[GlossaryEntry]]: [description]
        """
        raise NotImplementedError

class GlossaryEntry:
    """A GlossaryEntry represents a row in the Glossary.  
    """
    def __init__(self, form, gloss):
        self.form : str = form
        self.gloss : str = gloss
        self._tokens = []

    def get_size(self):
        """Return the expected size of tokens of this entry

        Raises:
            NotImplemented: [description]
        """
        raise NotImplementedError

    def tokens(self, timeline : 'Timeline' = None):
        """Confirmed tokens of the Glossary entry (in Timeline timeline)

        Args:
            timeline ([type], optional)r: [description]. Defaults to None.

        Raises:
            NotImplemented: [description]
        """
        return self._tokens

    def spot(self, timeline : 'Timeline' = None):
        """Detected tokens of Glossary Entry (in Timeline timeline)

        Args:
            timeline ([type], optional): [description]. Defaults to None.

        Raises:
            NotImplemented: [description]
        """
        raise NotImplementedError

    def breath_groups(self, confidence : float = None, timeline : 'Timeline'  = None) -> List[BreathGroup]:
        """Breath groups containing GlossaryEntry (>= confidence) (in Timeline timeline)

        Args:
            confidence (float, optional): [description]. Defaults to None.
            timeline (Timeline, optional): [description]. Defaults to None.

        Raises:
            NotImplemented: [description]
        """
        if not timeline:
            return sorted(list({t.breath_group for t in self._tokens}), key=lambda x: x.start)
        return sorted(list({t.breath_group for t in self._tokens if t.breath_group.timeline.identifier == timeline.identifier}), key=lambda x: x.start)

    def contains(self, glossary_entry : 'GlossaryEntry') -> None:
        """record that this GlossaryEntry has provided GlossaryEntry as a constituent

        Args:
            glossaryEntry2 ([type]): [description]
        
        Returns:
            None: [description]
        """
        raise NotImplementedError

    def cluster(self) -> List[List['Token']]:
        """Cluster tokens based on speech similarity

        Returns:
            List[List[Token]]: [description]
        """
        raise NotImplementedError

    def morphs(self) -> List[str]:
        """Get putative new forms based on detecting repeated speech material in tokens of existing entries

        Returns:
            List[str]: [description]
        """
        raise NotImplementedError

class Interperetation:
    """Interpretations are the text annotations on a span of a particular timeline.
    E.G., Translations, phonemic transcription, etc. 
    """
    def __init__(self, start_time, end_time, timeline, context) -> None:
        self.start_time : float = start_time
        self.end_time : float = end_time
        self.timeline : 'Timeline' = timeline
        self.context : str = context

class Token:
    """A Token is a pointer from the GlossaryEntry to the breath group(s) where the entry is found 
    """
    def __init__(self, glossary_entry, breath_group, confidence, start, end):
        self.glossary_entry = glossary_entry
        self.breath_group : 'BreathGroup' = breath_group
        self.confidence : float = confidence
        self.start = start
        self.end = end
        self.time = -1
        pass

    def __repr__(self) -> str:
        return str({
            "glossary_entry.gloss": self.glossary_entry.gloss, 
            "glossary_entry.form": self.glossary_entry.form, 
            "confidence": self.confidence, 
            "start": self.start, 
            "end": self.end,
            "breath_group.timeline.identifier": self.breath_group.timeline.identifier
            })

    def set_time(self, time : float) -> None:
        """Assign this Token a time value t  

        Args:
            t (float): duration of time (in seconds)
        """
        self.time = time






