### Sparse Transcription Data Model

This is the dev repo for the Sparse Transcription data model. 

The code for the model is in `sparse_transcription/data_model.py`.
The tests are in `tests/`.

To set up the project's virtual environment and install dependencies type `make`.

To run the tests, type `make test`.

To remove the virtual environment, type `make clean`.
