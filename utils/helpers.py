from datetime import datetime
import wave
import os
import hashlib
from textgrids import TextGrid
from sparse_transcription.data_model import Glossary, GlossaryEntry, STMedia

def load_textgrid(path_to_file : str, tier = None) -> TextGrid:
    # Try to open the file as textgrid
    try:
        grid = TextGrid(path_to_file)
    # Discard and try the next one
    except:
        raise Exception("Could not load textgrid file at ", path_to_file)
    
    if tier: # If tier is set, return the tier
        return grid[tier]
    return grid #Otherwise return the entire TextGrid

def load_example_glossary(path_to_file : str) -> Glossary:
    my_glossary = Glossary()
    try:
        with open(path_to_file, "r") as f:
            lines = f.readlines()
            for i, l in enumerate(lines):
                parts = l.split()
                form = parts[0]
                gloss = parts[1]
                new_glossary_entry = GlossaryEntry(form = form, gloss = gloss)
                my_glossary.add_entry(new_glossary_entry, id = i)
    except:
        raise Exception("Could not load test glossary file at ", path_to_file)
    return my_glossary

def load_media(path_to_media : str, timestamp : datetime ) -> STMedia:
    if path_to_media.endswith(".wav"):
        with wave.open(path_to_media, "r") as wav_file:
            nframes = wav_file.getnframes()  # Read n_frames new frames.
            framerate = wav_file.getframerate()
            return STMedia(
                id = path_to_media.split(os.path.sep)[-1],
                uri = path_to_media,
                frames = wav_file.readframes(nframes),     # Read n_frames new frames.
                timestamp = timestamp,
                metadata = {
                    "num_channels" : wav_file.getnchannels(),    # Number of channels. (1=Mono, 2=Stereo).
                    "sample_width" : wav_file.getsampwidth(),    # Sample width in bytes.
                    "framerate" : framerate,       # Frame rate.
                    "n_frames" : nframes,                        # Number of frames.
                    "comp_type" : wav_file.getcomptype(),        # Compression type (only supports "NONE").
                    "comp_name" : wav_file.getcompname(),        # Compression name.
                    "duration": nframes / framerate             # Calculate framerate
                }
            )
            
    else:
        raise Exception("Unable to handle media type: " + path_to_media)
    
