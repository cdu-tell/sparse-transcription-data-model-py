import pytest
import os
import hashlib
import datetime
from sparse_transcription.data_model import BreathGroup, Glossary, STProject, Timeline
from utils.helpers import load_example_glossary, load_media

def test_Timeline_init():
    """Test that the initialization of a Timeline works
    """
    media = load_media(
        os.path.join("resources", "example-mediastore", "MIXPRE-003-20180519-SG-tour-trim.TextGrid_1.wav"),
        timestamp=datetime.datetime(2002, 3, 11, 4, 30, 25)
    )
    new_timeline = Timeline(identifier="MIXPRE-003-20180519", media=media)

    assert new_timeline.identifier == "MIXPRE-003-20180519"
    assert new_timeline.media == media

def test_Timeline_add_breath_group():
    """Test insertion of breath groups to a timeline
    """
    media = load_media(
        os.path.join("resources","example-mediastore","MIXPRE-003-20180519-SG-tour-trim.TextGrid_3.wav"), 
        timestamp=datetime.datetime(2002, 3, 11, 4, 30, 25)
        )
    new_timeline = Timeline(identifier="MIXPRE-003-20180519", media=media)

    # Insert breathgroups, possibly out of sequential order
    new_timeline.add_breath_group(start=2.35, end=15.23)
    with pytest.raises(Exception): # Contains overlapping span, should raise error
        new_timeline.add_breath_group(start=15.22, end=17.23)   
    new_timeline.add_breath_group(start=17.24, end=21.23)
    new_timeline.add_breath_group(start=1.35, end=2.23)

    # Verify start and stop values of sorted breath_groups (sequential order of start times)
    assert(len(new_timeline.breath_groups) == 3)
    assert(sorted(new_timeline.breath_groups)[0].start == 1.35)
    assert(sorted(new_timeline.breath_groups)[0].end == 2.23)
    assert(sorted(new_timeline.breath_groups)[1].start == 2.35)
    assert(sorted(new_timeline.breath_groups)[1].end == 15.23)

def  test_BreathGroup_add_interpretations():
    # load MediaBundle
    path_to_audio_file = os.path.join("resources","example-mediastore", "MIXPRE-003-20180519-SG-tour-trim.TextGrid_0.wav")
    media = load_media(path_to_audio_file, timestamp=datetime.datetime(2002, 3, 11, 4, 30, 25))
    id = "003-20180519-SG"

    # create a Timeline object
    timeline = Timeline(identifier=id, media=media)

    # populate the timeline with breathgroups from the audio file
    # import pdb; pdb.set_trace()
    breath_group = BreathGroup(start=1.222, end= 3.555, timeline=timeline)

    # add an interpretation to a subspan of the breath group
    breath_group.add_interperetation(start=1.33, end=2.777, context="ngarribimbom")
    
    # we should be prevented from adding interpretations outside the bounds of the breath group
    with pytest.raises(Exception):
        breath_group.add_interperetation(start=1.22, end=2.777, context="ngarribimbom")
    with pytest.raises(Exception):    
        breath_group.add_interperetation(start=1.33, end=3.77, context="ngarribimbom")


def test_STMedia_init():
    """Test the creation of a media object given a pointer to a wav file in the media store
    """
    media_path = os.path.join('resources','example-mediastore','MIXPRE-003-20180519-SG-tour-trim.TextGrid_1.wav')
    media = load_media(media_path, timestamp=datetime.datetime(2002, 3, 11, 4, 30, 25))

    assert(media.id == 'MIXPRE-003-20180519-SG-tour-trim.TextGrid_1.wav')
    assert(media.uri == 'resources/example-mediastore/MIXPRE-003-20180519-SG-tour-trim.TextGrid_1.wav')
    assert(media.timestamp == datetime.datetime(2002, 3, 11, 4, 30, 25))
    assert(media.metadata['duration'] == 1.4271655328798185)
    assert(media.metadata['framerate'] == 22050)
    assert(media.metadata['n_frames'] == 31469)
    assert(media.metadata['sample_width'] == 2)
    assert(media.metadata['num_channels'] == 1)

def test_Glossary_init():
    """Test the creation of a glossary object from persisted txt file
    """
    glossary=load_example_glossary(os.path.join("specifications", "example-glossary.txt"))
    # Check Properties of SparseTranscription session
    assert(isinstance(glossary, Glossary))
    assert(glossary.entries[1].form == "korroko")
    assert(glossary.entries[1].gloss == "long.ago")

def test_STProject_simple_workflow():
    """Test the creation of a SparseTranscription project and start transcribing breath groups
    """

    # load Media object (STMedia)
    path_to_audio_file = os.path.join("resources","MIXPRE-030-20180608-RN-tour-trim.wav")
    media = load_media(path_to_audio_file, timestamp=datetime.datetime(2002, 3, 11, 4, 30, 25))
    media_collection_id = "030-20180519"

    # Initialize a project around a shared glossary
    project = STProject(
        glossary=load_example_glossary(os.path.join("specifications", "example-glossary.txt")),
        )

    # Create a new Timeline in the project (project.create_timeline(id : str, media : List[STMedia]))
    project.create_timeline(id = media_collection_id, media = [media])

    # Retrieve the Timeline and annotate it with BreathGroups 
    timeline = project.timelines[media_collection_id]
    timeline.add_breath_group(start = 0.0, end = 9.0) # Steven speaking in english
    timeline.add_breath_group(start = 9.1, end = 14.0) # RN speaking in Kunwinjku
    timeline.add_breath_group(start = 24.0, end = 29.0) # RN speaking in Kunwinjku
    timeline.add_breath_group(start = 30.0, end = 32.0) # RN speaking in Kunwinjku

    # Annotate a BreathGroup with a span to exclude from transcription
    timeline.breath_groups[0].exclude(start = 0.0, end = 9.0) # This entire BG is Steven speaking english, so we would like to ignore the whole thing.
    
    # Annotate part of a BreathGroup with an interpretation
    timeline.breath_groups[1].add_interperetation(start = 11.8, end = 13.1, context = "nawarddeken academy") # EG we understood the the tail end of this BG and would like to mark a translation.
    timeline.breath_groups[1].add_interperetation(start = 11, end = 11.8, context = "they name it") 

    # Sparsely transcribe sequences we recognize in the BreathGroup
    timeline.breath_groups[1].sparse_transcribe(start = 11, end = 11.8, form ="kabirringeybun" , gloss ="they_it.name.nonpast" , confidence = .88 , glossary = project.glossary) 
    timeline.breath_groups[1].sparse_transcribe(start = 11, end = 11.4, form ="kabirri" , gloss ="they_it" , confidence = .99 , glossary = project.glossary) 
    timeline.breath_groups[2].sparse_transcribe(start = 24.0, end = 24.4, form ="kabirri" , gloss ="they_it" , confidence = .61 , glossary = project.glossary) 
    timeline.breath_groups[2].sparse_transcribe(start = 27.5, end = 27.9, form ="kabirri" , gloss ="they_it" , confidence = .7 , glossary = project.glossary) 
    timeline.breath_groups[3].sparse_transcribe(start = 30.1, end = 30.5, form ="kabirri" , gloss ="they_it" , confidence = .75 , glossary = project.glossary) 

    # The glossary should at this point have 6 entries, the 4 from the sample glossary + 2 from the annotations above
    assert(len(project.glossary.entries) == 6)

    # We want to retrieve all BreathGroups containing instances of a particular entry where the form is "kabirri" and the gloss is "they_it" (ordered by start time)
    glossary_entry = project.glossary.get_entry(id = "57a0065162f7003dfa5cb21515f1a0d5")
    entry_bgs = glossary_entry.breath_groups()
    assert(len(entry_bgs) == 3)

    # From the timeline, get the concordances of a given GlossaryEntry
    concordance = timeline.concordance(glossary_entry)




    









