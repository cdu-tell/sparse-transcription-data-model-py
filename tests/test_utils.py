from utils.helpers import load_textgrid, load_example_glossary, load_media
import os
import datetime

def test_load_textgrid():
    tg = load_textgrid('resources/MIXPRE-030-20180608-RN-tour-trim.TextGrid')
    
    gold_xmin = 0 
    gold_xmax = 934.7194375 
    assert(gold_xmin == tg.xmin)
    assert(gold_xmax == tg.xmax)

    # if tier is provided, we load the textgrid tier
    tg = load_textgrid('resources/MIXPRE-030-20180608-RN-tour-trim.TextGrid', tier = "text")
    # Test that the list of Intervals has been properly read in
    # eg, an interval object looks like this:
    # <Interval text="" xmin=591.6879021863202 xmax=592.9347144147047>
    assert(len(tg) == 211)

def test_load_example_glossary():
    glossary = load_example_glossary('specifications/example-glossary.txt')
    assert(glossary.entries[0].form == "korroko")
    assert(glossary.entries[1].form == "korroko")
    assert(glossary.entries[2].form == "ba")
    assert(glossary.entries[3].form == "n/a")
    assert(glossary.entries[0].gloss == "just.now")
    assert(glossary.entries[1].gloss == "long.ago")
    assert(glossary.entries[2].gloss == "n/a")
    assert(glossary.entries[3].gloss == "be")

def test_load_media():
    media = load_media(
        os.path.join("resources", "example-mediastore", "MIXPRE-003-20180519-SG-tour-trim.TextGrid_0.wav" ),
        timestamp=datetime.datetime(2002, 3, 11, 4, 30, 25)
        )

    assert(str(media.timestamp)=='2002-03-11 04:30:25')
    assert(media.uri == "resources/example-mediastore/MIXPRE-003-20180519-SG-tour-trim.TextGrid_0.wav")
    assert(media.id == "MIXPRE-003-20180519-SG-tour-trim.TextGrid_0.wav")


 