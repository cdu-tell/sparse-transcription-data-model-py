venv: venv/touchfile

venv/touchfile: requirements.txt
	test -d venv || virtualenv -p python3 venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/touchfile

test: venv
	. venv/bin/activate; py.test tests

clean:
	rm -rf venv
	find -iname "*.pyc" -delete

enter-psql:
	sudo -i -u postgres && psql

show-datamodel-viz:
	pyreverse -my -A -o png -p data_model sparse_transcription/data_model.py

.PHONY: init test