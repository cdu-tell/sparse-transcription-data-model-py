import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sparse-transcription-wlane", # Replace with your own username
    version="0.0.1",
    author="William Lane and Steven Bird",
    author_email="william.lane@cdu.edu.au",
    description="Sparse transcription data model, reference implementation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sparse-transcription-data-model",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)